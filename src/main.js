import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'
import VueCurrencyFilter from 'vue-currency-filter'
import VueLocalStorage from 'vue-localstorage'


Vue.use(VueLocalStorage) 

// const store = new Vuex.Store({
//   // ...
//   plugins: [createPersistedState()]
// });
Vue.use(Buefy)
Vue.use(VueResource) 
Vue.http.options.crossOrigin = true

Vue.config.productionTip = false

Vue.use(VueCurrencyFilter,
  {
    symbol : '£',
    thousandsSeparator: '.',
    fractionCount: 2,
    fractionSeparator: ',',
    symbolPosition: 'front',
    symbolSpacing: true
  })

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
