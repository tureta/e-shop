import Vue from 'vue';
export const Storage = new Vue({
 data() {
     return {
      products: [],
      cart: [] 
     };
 },
 mounted () {
        const cart = JSON.parse(this.$localStorage.get('cart'))
        if (cart) {
        this.cart = cart
        }
    },
computed: {
    totalCost(){
     return this.cart.reduce((accum, product) => {
      return accum + product.details.price * product.quantity
     }, 0)
    },
    
   },
 methods: {
    getAllProducts() {
        axios.get("https://erply-challenge.herokuapp.com/list", {
            params:
            {
                AUTH: 'fae7b9f6-6363-45a1-a9c9-3def2dae206d',
            }}
        )
        .then(response => {
            this.products = !response.data ? [] : response.data})
            console.log(this.products)
        .catch(error =>{
            console.error(response.error)
        }
        );
        console.log(this.products)
        
    },

    addProductToCart:function(product) {
        const cartIndex = this.cart.findIndex(p => {return p.details.id === product.id})
        if(product.instock !== true){
            this.$toast.open({
                message: product.name + ' out of stock',
                type: 'is-danger'
            })
        }else{
            if(cartIndex === -1){
                this.cart.push(
                    {
                        details: product,
                        quantity: 1
                    })
                    console.log(this.cart[cartIndex])
                this.$localStorage.set('cart', JSON.stringify(this.cart))    
            }
            else{
                this.cart[cartIndex].quantity++
            }
        }
        
    },
    getAllitems:function(){
        
        this.$http.get('https://erply-challenge.herokuapp.com/list',{
         params: {
            AUTH: 'fae7b9f6-6363-45a1-a9c9-3def2dae206d'
         }
     })
     .then(
         response=>{
             return response.data
            //  this.products = !response.data? []: response.data
            //  console.log(response.data)
         }
     ).catch(error=> {
         return error
        //  console.log(error)
     })
    },
    removeProductFromCart: function (id) {
        const cartIndex = this.cart.findIndex(p => {
            return p.details.id === id
          })
          if(this.cart[cartIndex].quantity <= 1){
             this.cart.splice(cartIndex, 1)
          } else {
             this.cart[cartIndex].quantity--
             this.$localStorage.set('cart', JSON.stringify(this.cart))
          }
    },
    
   }

 
});